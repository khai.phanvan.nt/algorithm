// There is an idea how I solve this problem
// 
// For example we have a matrix like this:
// 
// [
//  [1, 2, 3],
//  [4, 5, 6],
//  [7, 8, 9]
// ]
// 
// If we want to rotate it 90 degres clockwise it will be come
// 
// [
//  [7, 4, 1],
//  [8, 5, 2],
//  [9, 6, 3]
// ]
// 
// As we can see the first row of the result matrix [7, 4, 1] is the first column of 
// the input matrix [1, 4, 7] but the element inside is in reverse order right?
// 
// So the solution is: get all the columns of the input matrix, and reverse 
// the order of all elements inside of it!
// 
// That's why I create a function to reverse the array name reverseMatrix()
// Then I create another function to extract every columns of that array name flipMatrix()
// Finally I create a compose function to wrap 2 functions above
// 
// But there is options here when we "flip" & "reverse" the matrix
// - When we reverse the original array first and then flip it, it will rotate "clockwise"
// - But we flip the matrix first and reverse it later it will rotate "counter clockwise"
// 
// In this case we want to rotate it clockwise so I will go for the first option
// 
// In the end we just need to check the K value to see how many time the matrix should rotate
// And the matrix in this case is a square with 4 edges so we can optimize it by using the modulus operator (%)
// to returns the division remainder value. And absolutely if the value equal to 0 there is no need to rotate
// 
// Complexity
// 
// reverseMatrix() function use Array.prototype.reverse (In place Algorithm) so it have 0(n) complexity
// flipMatrix() have 2 loops inside of it so it have O(n^2) complexity
// rotateClockwise() combine 2 above funtions so is have O(n^3) complexity
// 
// For rotateMatrix() it depends on K 
// If (K % 4) === 0 the complexity is O(1);
// If (K % 4) === 1 the complexity is O(n^3)
// If (K % 4) >= 2 the complexity is O(k + n^3) but whatever how big is K
// we already use the modulus operator to return the remainder value so the maxium value of K is 3
// 
// Sum up:
// - In the best case: O(1)
// - In the worst case: O(n^3)

const reverseMatrix = matrix => [...matrix].reverse();
const flipMatrix = matrix => matrix.map((col, idx) => matrix.map(row => row[idx]));
const rotateClockwise = matrix => flipMatrix(reverseMatrix(matrix));
// const rotateCounterClockwise = matrix => reverseMatrix(flipMatrix(matrix));

const rotateMatrix = (input, K = 1) => {
  if((K % 4) === 0) { 
    return input
  }
  let result = rotateClockwise(input);
  if (K >= 2) { 
    for (let i = 1; i < (K % 4); i++) { 
      result = rotateClockwise(result);
    }
  }
  return result;
};

module.exports = rotateMatrix;