// Inputs:
// - arrivals = [1, 3, 5]
// - departures = [2, 6, 10]
// - k = 1

// The way I solve this problem is:
// First I need to create to stove a number booking for each day
// with the maximum length equal to the last departure day (maxDate function) fill the day by 0 (no booking)
// then I loop through arrival days & create a loop (number of days to stay) between arrival day & departure day
// then I increase the booking at that day by 1 (dateRange[i]++)
// Finally I find the highest booking reveived in the date range by using again "maxDate" functions
// then compare it with "k" value (number of rooms) and return the result.
// 
// Complexity
// 
// maxDate() use Array.prototype.sort (In place Algorithm) so it have O(n) complexity
// 
// reservation()
// In the reservation function at first will create an array by call maxDate function so it will have 0(n) complexity
// Then it will create a loop through arrivals and create another loop between arrivals & departures so it will have O(n^2) complexity
// Finally it will return an operator by calling maxDate function again with O(n) complexity
// 
// Sum up: This algorithm have O(n^2) complexity



const maxDate = date => date.sort((a, b) => a - b)[date.length - 1];

const reservation = (arrivals, departures, k) => {
    const dateRange = new Array(maxDate(departures)).fill(0);
    arrivals.forEach((day, idx) => {
        const arrIdx = arrivals[idx] - 1;
        const depIdx = departures[idx] - 1;
        if (depIdx < arrIdx) {
            throw new Error('Departure day must be after arrival day');
        }
        for (let i = arrIdx; i < depIdx; i++) {
            dateRange[i]++;
        }
    });
    return maxDate(dateRange) <= k;
};

module.exports = reservation;