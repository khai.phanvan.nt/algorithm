const hotelReservation = require("./hotelReservation");

test("Conflict 1 room", () => {
  const arrivals = [1, 3, 5];
  const departure = [2, 6, 10];
  const room = 1;
  expect(hotelReservation(arrivals, departure, room)).toEqual(false);
});

test("No conflict 1 room large array", () => {
  const arrivals = [1, 3, 10000001];
  const departure = [2, 1000000, 10000002];
  const room = 1;
  expect(hotelReservation(arrivals, departure, room)).toEqual(true);
});

test("Conflict 2 rooms", () => {
  const arrivals = [1, 3, 5];
  const departure = [2, 6, 10];
  const room = 2;
  expect(hotelReservation(arrivals, departure, room)).toEqual(true);
});

test("No conflict 1 room", () => {
  const arrivals = [1, 3, 5];
  const departure = [2, 5, 10];
  const room = 1;
  expect(hotelReservation(arrivals, departure, room)).toEqual(true);
});

test("Error input", () => {
  const arrivals = [1, 3, 5];
  const departure = [2, 2, 10];
  const room = 1;
  expect(() => { hotelReservation(arrivals, departure, room) }).toThrow("Departure day must be after arrival day");
});
