const rotateMatrix = require("./rotateMatrix");

const square3 = [[0, 16, 255], [8, 128, 32], [0, 0, 0]];

const square4 = [[0, 16, 255, 1], [8, 128, 32, 2], [1, 2, 3, 4], [5, 6, 7, 8]];

test("3 angle Rotate 1 times", () => {
  const result = [
    [0, 8, 0],
	  [0, 128, 16],
    [0, 32, 255]
  ];
  expect(rotateMatrix(square3, 1)).toStrictEqual(result);
});

test("3 angle Rotate 2 times", () => {
  const result = [
    [0, 0, 0],
	  [32, 128, 8],
    [255, 16, 0]
  ];
  expect(rotateMatrix(square3, 2)).toStrictEqual(result);
});

test("3 angle Rotate 3 times", () => {
  const result = [
    [255, 32, 0],
	  [16, 128, 0],
    [0, 8, 0]
  ];
  expect(rotateMatrix(square3, 3)).toStrictEqual(result);
});

test("3 angle Rotate 4 times", () => {
  const result = [
    [0, 16, 255],
	  [8, 128, 32],
    [0, 0, 0]
  ];
  expect(rotateMatrix(square3, 4)).toStrictEqual(result);
});

test("3 angle Rotate 100 times", () => {
  const result = [
    [0, 16, 255],
	  [8, 128, 32],
    [0, 0, 0]
  ];
  expect(rotateMatrix(square3, 100)).toStrictEqual(result);
});

test("3 angle Rotate 1.000.000 times", () => {
  const result = [
    [0, 16, 255],
	  [8, 128, 32],
    [0, 0, 0]
  ];
  expect(rotateMatrix(square3, 1000000)).toStrictEqual(result);
});

test("3 angle Rotate 999.999.999 times", () => {
  const result = [
    [255, 32, 0],
	  [16, 128, 0],
    [0, 8, 0]
  ];
  expect(rotateMatrix(square3, 999999999)).toStrictEqual(result);
});

test("3 angle Rotate 1.000.000.000 times", () => {
  const result = [
    [0, 16, 255],
	  [8, 128, 32],
    [0, 0, 0]
  ];
  expect(rotateMatrix(square3, 1000000000)).toStrictEqual(result);
});

test("4 angle Rotate 1 times", () => {
  const result = [
    [5, 1, 8, 0],
    [6, 2, 128, 16], 
    [7, 3, 32, 255], 
    [8, 4, 2, 1]
  ];
  expect(rotateMatrix(square4, 1)).toStrictEqual(result);
});

test("4 angle Rotate 2 times", () => {
  const result = [
    [8, 7, 6, 5],
    [4, 3, 2, 1], 
    [2, 32, 128, 8], 
    [1, 255, 16, 0]
  ];
  expect(rotateMatrix(square4, 2)).toStrictEqual(result);
});
